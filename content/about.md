+++
date = "2015-08-22T06:42:21-07:00"
draft = false
title = "About Me"

+++

I am Kapil Kaisare, a software engineer currently working on [SlingTv](http://www.slingtv.com/). I am interested in software design, programming paradigms, operating systems, information retrieval and artificial intelligence.

This is the umpteenth time I am trying to setup a site. May I be delivered from lethargy.

While serving as a log of my programming explorations, this blog will also record my thoughts on other subjects including history, politics and science.