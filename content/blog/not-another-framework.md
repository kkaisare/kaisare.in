+++
draft = false
date = "2016-10-13T20:31:39-06:00"
title = "Not another framework!"

+++

After a little more than two months of working with it, I continue to remain unimpressed with Angular 2. It appears to me an overengineered artefact, which when coupled with webpack results in an intolerable mess.

One surprise is how it doesn't seem to be built with the framework independent Polymer in mind. The only way to use Polymer is via a third party plugin; one would expect Polymer elements to be first class citizens in a Google framework.

More annoying however, are error messages that do not help the programmer in understanding what their mistake is. For instance, an 'untyped argument' error was resolved by fixing a route name. It suggests that the framework has been built in a manner which renders capturing programmer intent difficult, if not impossible. To me, that is bad design.

It's not all bad, mind you. Angular 2 is the first framework I know of which embraces Typescript, whose strong types eliminate a whole class of unit tests. And being able to define framework independent models offers a path to migrating to other frameworks, should one choose to. And then there's virtual DOM, although that is pretty much old hat at this stage...

I've seen frameworks come and go. None linger long enough in my opinion, to justify their 'framework-calls-the-application' approach to development. And I am oft reminded of Robert Martin's exhortation to build one's application to be independent of frameworks, reducing them to mere presentation layers alone. Alistair Cockburn's [hexagonal architecture](http://alistair.cockburn.us/Hexagonal+architecture) is an excellent idea, and I'd love to try implementing one soon.