+++
title = "Site Update"
draft = false
date = "2016-10-15T23:04:30-06:00"

+++

I've cleaned out the structure for the front page; all it does now is list the posts of content type "blog". Other content types planned for include projects and fiction.