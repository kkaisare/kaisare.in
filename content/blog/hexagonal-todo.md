+++
title = "A hexagonal todo"
draft = false
date = "2016-10-14T19:44:47-06:00"

+++

To flesh out my ideas around hexagonal architecture, I am going to build a simple todo application in that fashion.

The core application will be pure javascript; it will offer commands that views can use to drive it. Views, when they register with the app, also specify event listeners for predetermined events.

To go all the way, I will build three views; one in Angular 2, one in Backbone, and a CLI. Data will be persisted across two databases: Mongo and SQLite.