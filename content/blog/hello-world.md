+++
draft = false
title = "Hello World!"
date = "2016-10-12T19:35:01-06:00"

+++

This is the umpteenth time I am trying to setup a site. May I be delivered from lethargy.

While serving as a log of my programming explorations, this blog will record my thoughts on other subjects including history, politics and science.
