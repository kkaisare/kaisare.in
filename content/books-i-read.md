+++
draft = false
title = "Books I read"
date = "2016-10-12T19:35:01-06:00"

+++

## 2016
* *[Precalculus Mathematics in a Nutshell](https://www.amazon.com/Precalculus-Mathematics-Nutshell-Geometry-Trigonometry/dp/1592441300/) - George Simmons*
* *[Old Man's War](https://www.amazon.com/Old-Mans-War-John-Scalzi/dp/0765348276) - John Scalzi*   A military sci-fi novel that reminds me of Robert Heinlein. Extraordinary for a debut. 4/5.
* *[End of an Era](https://www.amazon.com/End-Era-Robert-J-Sawyer/dp/0312876939/) - Robert Sawyer* Thin plot and characters, thinner science. 2/5.